---
title: "About"
image: "profile.jpg"
weight: 8
---

In 2016, I started building web and mobile applications with all kinds of modern stacks. Driven by a will to create the simplest solutions, I try to learn something new every day.

### My skills

* I especially like working with **Vue**, **Flutter** and **Firebase**
* CI/CD are no strangers to me: I run my tests with **Glitab CI** to ensure a good overall code quality
* Frequently coupling **Docker** and **Kubernetes** to fasten the deployment process
* While working with a team, we (try to) follow the **SCRUM** guidelines