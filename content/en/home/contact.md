---
title: "Interested in collaborating ?"
---

I'm open to new opportunities, especially in **Melbourne** !

I also do some **freelance** work, feel free to check out my [Malt](https://www.malt.com/profile/audricmanaud) profile.
