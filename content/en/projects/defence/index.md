---
title: "Defence"
weight: 50
---

Since 2018, I have a full-time job as a software engineer for the French Ministry of Armed Forces. We develop mission-critical systems that must not fail us, and therefore require a lot of testing, fast rollouts and high-availability (hi Kubernetes).
