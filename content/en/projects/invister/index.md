---
title: "Invister"
weight: 10
---

A management solution for football scouts to enhance their recruitment processes. It comes with both a web and mobile application, a full-text search, realtime sync and a mail notification system. This one was made using Flutter, Firebase and React. It also leverages Algolia and Twilio's APIs.